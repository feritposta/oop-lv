#include <iostream>

using namespace std;

class Vector
{
public:
	friend Vector operator +(const Vector&, const Vector&);
	friend bool operator ==(const Vector&, const Vector&);
	friend ostream& operator<<(ostream&, const Vector&);
	Vector& operator =(const Vector&);
	Vector();
	Vector(float, float, float);
	~Vector();

private:
	float mi, mj, mk;
};

Vector operator +(const Vector& r, const Vector& l) { return Vector(r.mi + l.mi, r.mj + l.mj, r.mk + l.mk); }

bool operator ==(const Vector& r, const Vector& l) { return r.mi == l.mi && r.mj == l.mj && r.mk == l.mk; }

ostream& operator<<(ostream& out, const Vector& r) { out << r.mi << "i + " << r.mj << "j + " << r.mk << "k"; return out; }

Vector& Vector::operator =(const Vector& r) { mi = r.mi; mj = r.mj; mk = r.mk; return *this; }

Vector::Vector() : mi(-1), mj(-1), mk(-1) { }

Vector::Vector(float i, float j, float k) : mi(i), mj(j), mk(k) { }

Vector::~Vector() { }

int main() {

	Vector first(2, 3, 4), second(5, 6, 7), third, fourth;

	third = first + second;

	fourth = first;

	cout << first << endl;
	cout << second << endl;
	cout << third << endl;
	cout << fourth << endl;
	bool var = first == second;
	cout << var << endl;

	return 0;
}