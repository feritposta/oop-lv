#define _USE_MATH_DEFINES
#include <iostream>

using namespace std;

float Area(float r) {
	return r * r * M_PI;
}
float Area(float a, float b) {
	return a * b;
}
double Area(double a, double b) {
	return a * b / 2;
}

int main() {

	cout << Area(2) << endl;
	cout << Area((float)2.3, (float)2.5) << endl;
	cout << Area(2.3, 2.5) << endl;

	return 0;
}