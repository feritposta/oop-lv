//#include "headeric.h"

#include <iostream>
using namespace std;

class Poly
{
public:
	friend Poly operator+(const Poly&, const Poly&);
	friend bool operator==(const Poly&, const Poly&);
	friend ostream& operator<<(ostream&, const Poly&);
	Poly();
	Poly(float, float, float);
	~Poly();

private:
	float mx_2;
	float mx;
	float mind;
};

Poly operator+(const Poly& r, const Poly& l) { return Poly(r.mx_2 + l.mx_2, r.mx + l.mx, r.mind + l.mind); }

bool operator==(const Poly& r, const Poly& l) { return r.mx_2 == l.mx_2 && r.mx == l.mx && r.mind == l.mind; }

ostream& operator<<(ostream& out, const Poly& r) { out << r.mx_2 << ' ' << r.mx << ' ' << r.mind; return out; }

Poly::Poly() : mx_2(1), mx(1), mind(1) { }

Poly::Poly(float a, float b, float c) : mx_2(a), mx(b), mind(c) { }

Poly::~Poly() { }