/*
Napi�ite klasu �Contact� koja predstavlja Contact u telefonskom imeniku s
podacima nu�nim za odr�avanje takvog imenika (ime i prezime, telefonski broj,
e-mail). Kreirajte polje objekata i u njega smjestite tri objekta stvorena
parametarskim konstruktorom. Otvorite datoteku (ime se unosi s tipkovnice) i
u nju kori�tenjem preoptere�enog operatora za ispis upi�ite sadr�aj ranije
kreiranog polja. Za sve potrebe rada s nizovima znakova koristiti string klasu.
*/

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class Contact
{
public:
	Contact();
	Contact(string, string, string);
	~Contact();
	friend ostream& operator << (ostream &, const Contact &);

private:
	string mname;
	string mmail;
	string mnumber;
};

Contact::Contact() { }

Contact::Contact(string name, string mail, string number) : mname(name), mmail(mail), mnumber(number) { }

Contact::~Contact() { }

ostream& operator <<(ostream& out, const Contact& ref) {
	out << ref.mname << "  " << ref.mmail << "  " << ref.mnumber << endl;
	return out;
}

int main() {

	Contact *phbook = new Contact[3]{
		Contact("Ana Anic", "ana.anic@etfos.hr", "0978764532"),
		Contact("Ivo Ivic", "ivo.ivic@etfos.hr", "0987545634"),
		Contact("Marko Markic", "marko.markic@etfos.hr", "0872134543")
	};

	string filename;
	cout << "Enter filename: ";
	getline(cin, filename);
	ofstream file(filename);

	for (int i = 0; i < 3; i++) {
		file << phbook[i];
	}

	file.close();
	delete[] phbook;

	return 0;
}