#include <iostream>
#include <string>
using namespace std;

int find_indx(int arr[], int size);
int find_indx(double arr[], int size);
int find_indx(char arr[], int size);
int find_indx(string arr[], int size);

int main() {

	cout << "cheese" << endl;

	return 0;
}


int find_indx(int arr[], int size) {
	int min = arr[0], ret_val = 0;
	for (int i = 0; i < size; i++) {
		if (arr[i] < min) {
			min = arr[i];
			ret_val = i;
		}
	}
	return ret_val;
}

int find_indx(double arr[], int size) {
	double min = arr[0];
	int ret_val = 0;
	for (int i = 0; i < size; i++) {
		if (arr[i] < min) {
			min = arr[i];
			ret_val = i;
		}
	}
	return ret_val;
}

int find_indx(char arr[], int size) {
	char min = arr[0];
	int ret_val = 0;
	for (int i = 0; i < size; i++) {
		if (arr[i] < min) {
			min = arr[i];
			ret_val = i;
		}
	}
	return ret_val;
}

int find_indx(string arr[], int size) {
	string min = arr[0];
	int ret_val = 0;
	for (int i = 0; i < size; i++) {
		if (arr[i] < min) {
			min = arr[i];
			ret_val = i;
		}
	}
	return ret_val;
}