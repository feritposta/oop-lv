/*
Definirajte klasu koja predstavlja bankovni teku�i ra�un s atributima za stanje,
kamatu i limit, ima podrazumijevani i parametarski konstruktor, pristupne
metode za postavljanje i dohva�anje atributa, metode za uplatu i isplatu
(voditi ra�una o limitu), metodu za pripis kamate te metodu za ispis stanja.
Povesti ra�una o enkapsulaciji.
*/


#include<iostream>
using std::endl;
using std::cout;
using std::cin;

class Account
{
public:
	Account();
	Account(float, float, float);
	~Account();
	void set_balance(float);
	void set_limit(float);
	void set_interest(float);
	float get_balance() const;
	float get_limit() const;
	float get_interest() const;
	void payment(float);
	void withdrawal(float);
	void print_balance() const;
	void add_interest();
private:
	float mbalance, mlimit, minterest;
};

Account::Account() : mbalance(0), mlimit(-1000), minterest(0.0001) { }

Account::Account(float balance, float limit, float interest) : mbalance(balance), mlimit(limit), minterest(interest) { }

Account::~Account() { }

void Account::set_balance(float balance) { mbalance = balance; }

void Account::set_limit(float limit) { mlimit = limit; }

void Account::set_interest(float interest) { minterest = interest; }

float Account::get_balance() const { return mbalance; }

float Account::get_limit() const { return mlimit; }

float Account::get_interest() const { return minterest; }

void Account::payment(float payment) { mbalance += payment; }

void Account::withdrawal(float withdrawal) { mbalance -= withdrawal; }

void Account::print_balance() const { cout << "Curr acc balance is: " << mbalance << endl; }

void Account::add_interest() { mbalance = (mbalance * (1 + minterest)); }

int main(void) {

	Account *r = new Account;

	r->set_balance(2500);
	r->set_limit(-2000);
	r->set_interest(0.01);
	cout << "balance: " << r->get_balance() << endl;
	cout << "limit: " << r->get_limit() << endl;
	cout << "interest: " << r->get_interest() << endl;
	r->print_balance();
	r->add_interest();
	r->print_balance();

	r->withdrawal(1000);
	r->payment(10);
	r->print_balance();

	delete r;
	return 0;
}