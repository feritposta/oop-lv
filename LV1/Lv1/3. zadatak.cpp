#include<iostream>
using std::cout;
using std::cin;
using std::endl;

class Dot {
private:
	int mx, my;
public:
	Dot();
	Dot(int, int);
	~Dot() { };
	int get_x(), get_y();
	void set_cor(int, int);
};

class Line {
private:
	Dot mT1, mT2;
public:
	Line(Dot, Dot);
	~Line() { };
	Dot get_T1();
	Dot get_T2();
	void set_Dots(Dot, Dot);
	void print_Dots();
};

Dot::Dot() : mx(1), my(1) { }

Dot::Dot(int x, int y) : mx(x), my(y) { }

int Dot::get_x() { return mx; }

int Dot::get_y() { return my; }

void Dot::set_cor(int x, int y) { mx = x, my = y; }

Line::Line(Dot T1, Dot T2) { mT1 = T1; mT2 = T2; }

Dot Line::get_T1() { return mT1; }

Dot Line::get_T2() { return mT2; }

void Line::print_Dots() {
	cout << "T1 (" << mT1.get_x() << ", " << mT1.get_y() << ")" << endl;
	cout << "T2 (" << mT2.get_x() << ", " << mT2.get_y() << ")" << endl;
}

void Line::set_Dots(Dot a, Dot b) { mT1 = a; mT2 = b; }

int main(void) {

	Dot a(0, 0);
	Dot b(1, 1);
	Line p(a, b);
	p.print_Dots();

	a.set_cor(1, 2);
	b.set_cor(2, 3);
	p.set_Dots(a, b);
	p.print_Dots();

	return 0;
}