#include<iostream>
#include<time.h>
using std::cout;
using std::cin;
using std::endl;

class Kocka {
private:
	int *side = new int[6];
public:
	void set_sides(int, int, int, int, int, int);
	int* get_sides();
	int throw_cube();
	Kocka();
	Kocka(int, int, int, int, int, int);
	~Kocka() { delete side; cout << "Kocka unistena" << endl; };
};

Kocka::Kocka() {
	for (int i = 0; i <= 5; i++) {
		side[i] = i + 1;
	}
	cout << "Kocka napravljena" << endl;
}

Kocka::Kocka(int a, int b, int c, int d, int e, int f) {
	int arr[] = { a,b,c,d,e,f };
	for (int i = 0; i <= 5; i++) {
		side[i] = arr[i];
	}
	cout << "Kocka napravljena" << endl;
}

int* Kocka::get_sides() {
	return side;
}

void Kocka::set_sides(int a, int b, int c, int d, int e, int f) {
	int arr[] = { a,b,c,d,e,f };
	for (int i = 0; i <= 5; i++) {
		side[i] = arr[i];
	}
}

int Kocka::throw_cube() {
	return side[rand() % 5];
}

int main(void) {

	srand((unsigned)time(NULL));

	Kocka arr[6];	

	int *arr_rez = new int[6];
	for (int i = 0; i < 6; i++) {
		arr_rez[i] = arr[i].throw_cube();
	}

	for (int i = 0; i < 6; i++) {
		if (arr_rez[0] != arr_rez[i]) {
			cout << "Nis od jamba." << endl;
			return 0;
		}
	}
	cout << "Jamb! Bravo." << endl;

	return 0;
}