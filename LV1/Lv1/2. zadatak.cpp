#include<iostream>
using std::cout;
using std::cin;
using std::endl;

class Complex {
	friend Complex zbroji(Complex &, Complex &);
private:
	float mre, mim;
public:
	Complex();
	Complex(float, float);
	~Complex() {};
	float get_re();
	float get_im();
	void set_num(float, float);
	float get_module();
};

Complex::Complex() { mre = 1; mim = 1; }

Complex::Complex(float re, float im) : mre(re), mim(im) { }

float Complex::get_re() { return mre; }

float Complex::get_im() { return mim; }

void Complex::set_num(float re, float im) { mre = re; mim = im; }

float Complex::get_module() { return sqrt(mre * mre + mim * mim); }

Complex zbroji(Complex &re1, Complex &re2) {
	Complex rez;
	rez.mre = re1.mre + re2.mre;
	rez.mim = re1.mim + re2.mim;
	return rez;
}

int main(void) {

	Complex br1, br2, rez;
	Complex &ref1 = br1, &ref2 = br2;
	rez = zbroji(ref1, ref2);
	cout << "Modul: " << ref1.get_module() << endl;
	cout << "Realno: " << rez.get_re() << endl << "Imaginarno: " << rez.get_im() << endl;

	return 0;
}