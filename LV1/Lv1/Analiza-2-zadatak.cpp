/*
Definirajte klasu koja predstavlja pravokutnik, omogu�uje pristup atributima
te nudi mogu�nost ra�unanja povr�ine i opsega pravokutnika. Napisati
funkciju koja uspore�uje tri pravokutnika po veli�ini i ispisuje povr�inu
najve�eg. Povesti ra�una o enkapsulaciji.
*/

#include<iostream>
using std::endl;
using std::cout;
using std::cin;

class Rectangle
{
public:
	Rectangle();
	~Rectangle();
	Rectangle(float, float);
	float get_a() const;
	float get_b() const;
	void set_a(float);
	void set_b(float);
	float surface() const;
	float circ() const;
private:
	float ma, mb;
};

Rectangle::Rectangle() : ma(1), mb(1) { }

Rectangle::~Rectangle() { }

Rectangle::Rectangle(float a, float b) : ma(a), mb(b) { }

float Rectangle::get_a() const { return ma; }

float Rectangle::get_b() const { return mb; }

void Rectangle::set_a(float a) { ma = a; }

void Rectangle::set_b(float b) { mb = b; }

float Rectangle::surface() const { return ma * mb; }

float Rectangle::circ() const { return 2 * ma + 2 * mb; }

void print_lrg_area(Rectangle *a, Rectangle *b, Rectangle *c) {
	float a_area = a->surface(), b_area = b->surface(), c_area = c->surface();
	if (a_area > b_area && a_area > c_area) cout << "The biggest area is: " << a_area << endl;
	else if (b_area > a_area && b_area > c_area) cout << "The biggest area is: " << b_area << endl;
	else if(c_area > a_area && c_area > b_area) cout << "The biggest area is: " << c_area << endl;
	else cout << "Wild err here..." << endl;
}

int main() {

	Rectangle *a = new Rectangle(2,2);
	Rectangle *b = new Rectangle(3,3);
	Rectangle *c = new Rectangle;

	print_lrg_area(a, b, c);

	delete a, b, c;

	return 0;
}